#define ALPHA_EM (1.0/137.035989)
#define EMASS 0.51099906e-3 /* GeV */
#define MUMASS 105.658389e-3 /* GeV */

#define C 299792458.0 /* m/s */
/* #define HBAR 1.05457266e-34*/ /* J s */
#define HBAR 6.582122e-25 /* GeV s */
#define RE 2.81794092e-15 /* m */
#define LAMBDA_BAR 3.86159323e-13

#define GeV2J 1.60217733e-10
